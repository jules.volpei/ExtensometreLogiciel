import queue
import sys

from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSignal, QThread
import cv2 as cv
import numpy as np
from PyQt5.QtWidgets import QErrorMessage, QMessageBox

import Extensometre.utils.camera.cameraSetup as camera

dataRoi = {'position1': None, 'position2': None}
finito = False

def dessineRectangle(event, x, y, flags, param):
    """
    Fonction permettant de dessiner la zone de sélection manuelle sur la fenêtre OpenCV.

    Paramètres :
        - event : Type d'évènemment (EVENT_LBUTTONDOWN, EVENT_MOUSEMOVE, EVENT_LBUTTONUP)
        - x : Coordonnée en x du curseur sur l'image.
        - y : Coordonnée en y du curseur sur l'image.
        - flags : ???
        - param : ???
    """
    # On définit ces trois variables globales dans le fichier
    global dataRoi, drawing, dessinFini
    x *= 2
    y *= 2

    # Si l'utilisateur appuie sur son clic gauche
    if event == cv.EVENT_LBUTTONDOWN:
        # L'utilisateur est en train de dessiner le rectangle
        drawing = True
        # On initialise les coordonnées de départ du rectangle
        dataRoi['position1'] = (x, y)
    elif event == cv.EVENT_MOUSEMOVE:
        # L'utilisateur bouge sa souris
        if drawing:     # Si l'utilisateur est en train de dessiner
            # On commence à dessiner le rectangle
            cv.rectangle(frameROI, dataRoi['position1'], (x, y), (0, 255, 0), 2)
    elif event == cv.EVENT_LBUTTONUP:
        # L'utilisateur relâche son clic gauche
        drawing = False
        # On termine de dessiner le rectangle
        cv.rectangle(frameROI, dataRoi['position1'], (x, y), (0, 255, 0), 2)
        # On enregistre les coordonnées de la fin du rectangle
        dataRoi['position2'] = (x, y)
        finito = True


class VideoThread(QThread):
    """
    Classe permettant l'acquisition et la transmission du rendu vidéo du logiciel.
    """
    changePixmapSignalFrame = pyqtSignal(tuple)
    cameraQueue = queue.Queue(maxsize=5)
    __numCam = camera.ChoixCamera().getVideoSources()
    __cap = cv.VideoCapture()
    __distance = 0
    __tailleContours = {}
    __indiceCouleurMarques = 0
    __minimumTaille = 300
    __listeCouleursMarque = ([
        (np.array([0, 0, 0]), np.array([180, 255, 50])),  # Valeurs de noir
        (np.array([0, 0, 220]), np.array([255, 255, 255]))  # Valeurs de blanc
    ])
    __hue = 1
    __saturation = 1
    __value = 1
    __indiceEnregistrementFrame = False
    __indiceEnvoieDonnees = False
    __temps = 0
    __indiceReductionDeBruit = False
    __fps = 0
    __indiceSelectionAuto = False
    __indiceSelectionFinie = False
    __dimensionFrame = None
    __fenetreSelectionManuelleCree = False
    __fenetreSelectionManuelleSupprimee = False
    __dessinFait = False
    __dossierFrame = None
    __noSignal = False

    def __init__(self, nomCam=None, fps=20):
        """
        Paramètres :
            - nomCam : Nom de la caméra sur laquelle lancer la capture vidéo
            - fps : Nombre de frames par secondes (pas implémenté, 20 par défaut)
        """
        super().__init__()
        print(camera.ChoixCamera().getVideoSources())

        if len(camera.ChoixCamera().getVideoSources()) == 0:
            fenetreErreur = QMessageBox()
            fenetreErreur.setIcon(QMessageBox.Critical)
            fenetreErreur.setWindowTitle("Erreur")
            fenetreErreur.setText(
                "Le programme ne détecte pas de caméras. Veuillez-vous assurer qu\'une caméra est branchée à l\'ordinateur de test pour lancer le logiciel."
            )
            sys.exit(fenetreErreur.exec_())
        if nomCam is None:
            nomCams = [i for i in camera.ChoixCamera().getVideoSources().keys()]
            self.__numCam = camera.ChoixCamera().getVideoSources()[nomCams[0]]
            self.__fps = fps
            self.__noSignal = False
        else:
            self.__numCam = camera.ChoixCamera().getVideoSources()[nomCam]
            self.__fps = fps
            self.__noSignal = False

    def run(self):
        """
        Méthode permettant de démarrer la capture vidéo en fonction de la source donnée par l'utilisateur
        Cette méthode ne retourne pas directement des valeurs mais elle donne quatre paramètres à un signal
        PyQt relié à l'objet permettant d'afficher la vidéo. Ces quatre paramètres sont :
            - frame, correspondant à la matrice de l'image en cours.
            - distancePX, distance calculée par le logiciel entre les deux groupes de pixels correspondant aux marques.
            - tailleContours, dictionnaire avec en valeur la taille en pixels du contour des tâches.
            - coordonneesCercles, dictionnaire avec en valeur les coordonnées des deux groupes de pixels.
        """
        global frameROI     # Variable globale

        # On initialise le lancement de la capture
        self.isRunning = True
        self.__cap = cv.VideoCapture(self.__numCam)
        self.__cap.set(cv.CAP_PROP_FPS, self.__fps)
        self.__fps = self.__cap.get(cv.CAP_PROP_FPS)
        try:
            # Redimensionnement du rendu vidéo (adapté à la DMK 37BUX226)
            self.__cap.set(cv.CAP_PROP_FRAME_WIDTH, 4000)
            self.__cap.set(cv.CAP_PROP_FRAME_HEIGHT, 3000)
        except:
            print("Marche pas sur la cam")
        while True:
            ret, image = self.__cap.read()
            if not ret:
                self.__noSignal = True
            else:
                self.__dimensionFrame = image.shape
                if ret:
                    # On récupère les données de la méthode de traitement
                    frame, distancePX, tailleContours, coordonneesCercles = self.traitementImage(image)
                    # Ajout de la frame à la queue pour la fenêtre OpenCV
                    self.cameraQueue.put(frame[0])

                    # Si l'utilisateur sélectionne manuellement une zone
                    if self.__indiceSelectionAuto:
                        # On commence par créer la fenêtre OpenCV. Comme c'est une boucle on fait en sorte de la créer qu'une fois
                        self.__fenetreSelectionManuelleSupprimee = False
                        if not self.__fenetreSelectionManuelleCree:
                            self.__fenetreSelectionManuelleCree = True
                            cv.namedWindow('PRESS Q TO CLOSE')
                            # On ajoute la méthode pour dessiner la zone de sélection
                            cv.setMouseCallback('PRESS Q TO CLOSE', dessineRectangle)
                        if dataRoi['position1'] is not None and dataRoi['position2'] is not None:
                            # Si la zone est sélectionnée par l'utilisateur
                            self.__dessinFait = True
                            frameROI = self.cameraQueue.get()
                        else:
                            # On récupère par défaut l'image dans la queue pour la fenêtre OpenCV
                            frameROI = self.cameraQueue.get()
                        # Redimensionnement de la fenêtre OpenCV.
                        test = cv.resize(frameROI, (self.__dimensionFrame[1]//2, self.__dimensionFrame[0]//2))
                        cv.imshow('PRESS Q TO CLOSE', test)

                        # Permet de fermer la zone de sélection avec la lettre q. (Il vaut mieux décocher la case sur le logiciel).
                        if cv.waitKey(1) & 0xFF == ord('q'):
                            cv.destroyWindow('PRESS Q TO CLOSE')
                            self.__fenetreSelectionManuelleSupprimee = True
                            self.__fenetreSelectionManuelleCree = False
                    else:   # Si l'utilisateur n'a pas coché la sélection manuelle
                        frameROI = self.cameraQueue.get()
                        if self.__dessinFait:
                            self.adapteZone(coordonneesCercles)
                        # On déruit la fenêtre de sélection manuelle.
                        if self.__fenetreSelectionManuelleCree and not self.__fenetreSelectionManuelleSupprimee:
                            cv.destroyWindow('PRESS Q TO CLOSE')
                            self.__fenetreSelectionManuelleSupprimee = True
                            self.__fenetreSelectionManuelleCree = False
                    # On initialise les données de distance et de tailles des groupes de pixels
                    self.__distance = distancePX
                    self.__tailleContours = tailleContours
                    # On émet la frame avec un signal PyQt
                    self.changePixmapSignalFrame.emit(frame)

    def traitementImage(self, frame):
        """
        Methode lancée sur sur un thread permettant d'obtenir un masque détectant le noir sur les frames passées dans la queue.

        Paramètre :
            - frame : Image actuelle.

        Return de la méthode :
            - (frame, masque), tuple comportant l'image actuelle sous forme de frame avec les modifications du programme ainsi que le masque.
            - distancePX, distance en pixels mesurée par le programme.
            - dictPointTaille, dictionnaire comportant chaque point comme clé avec son contour comme valeur.
            - coordonneesCercles, dictionnaire comportant pour chaque groupe de pixels en tant que clé les coordonnées de son centre en valeur.
        """
        # Si la sélection automatique n'a pas été coché par l'utilisateur
        if not self.__indiceSelectionAuto:
            frame = frame
            if self.__dessinFait:  # Si la sélection a déjà été faite
                # Min et max important, selon comment le rectangle est fait par l'utilisateur
                frame = frame[min(dataRoi['position1'][1], dataRoi['position2'][1]):max(dataRoi['position1'][1],
                                                                                        dataRoi['position2'][1]),
                        min(dataRoi['position1'][0], dataRoi['position2'][0]):max(dataRoi['position1'][0],
                                                                                  dataRoi['position2'][0])]

        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)  # Changement d'espace colorimétrique

        # On découpe les vecteurs correspondant aux valeurs de hue, saturation et value
        h, s, v = cv.split(hsv)

        # On ajoute comme poids à ces vecteurs la valeur de chaque attribut correspondant à la hue, saturation et valeur
        # pour modifier les vecteurs. On passe par des points car ce sont des vecteurs.
        h = cv.addWeighted(h, self.__hue, 0, 1 - self.__hue, 0)
        s = cv.addWeighted(s, self.__saturation, 0, 1 - self.__saturation, 0)
        v = cv.addWeighted(v, self.__value, 0, 1 - self.__value, 0)

        # On regroupe les trois vecteurs
        nouveauHsv = cv.merge([h, s, v])

        frame = cv.cvtColor(nouveauHsv, cv.COLOR_HSV2BGR)

        # Création du masque en indiquant les valeurs min et max de noirs
        masque = cv.inRange(nouveauHsv, self.__listeCouleursMarque[self.__indiceCouleurMarques][0], self.__listeCouleursMarque[self.__indiceCouleurMarques][1])

        # Si l'utilisateur veut activer la réduction de bruit
        if self.__indiceReductionDeBruit:
            kernel = np.ones((5, 5), np.uint8)
            masqueDilatation = cv.erode(masque, kernel, iterations=2)
            masque = cv.dilate(masqueDilatation, kernel, iterations=2)

        # Création des contours pour sur le masque directement
        contours = cv.findContours(masque, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)[0]

        # On filtre pour n'avoir que les contours dont la taille est supérieure à la taille minimale
        contoursFiltres = [cnt for cnt in contours if cv.contourArea(cnt) > self.__minimumTaille]

        # Dictionnaire pour stocker les coordonnées des points
        coordonneesCercle = {}
        cmpt = 0  # Compteur pour donner les clés
        # On parcourt tous les contours détecté, soit les regroupement de pixels noirs ayant la taille minimale
        dictPointTaille = {}
        for contour in contoursFiltres:
            point = 'y' + str(cmpt + 1)     # Nom du points (y1, y2, y3, ...)
            dictPointTaille[point] = cv.contourArea(contour)
            # On récupère les valeurs du bouding par rapport au contour passé
            x, y, largeur, hauteur = cv.boundingRect(contour)
            # On dessine un rectangle avec les coordonnées associées
            cv.rectangle(frame, (x, y), (x + largeur, y + hauteur), (0, 0, 255), 2)
            # On ajoute les coordonnées au dictionnaire
            coordonneesCercle[cmpt] = (x + largeur // 2, y + hauteur // 2)
            cmpt += 1
            # On dessine un cercle au centre du rectangle
            # cv.circle(frame, (x + largeur // 2, y + hauteur // 2), 5, (0, 0, 255), -1)
            cv.putText(frame, point, (x + largeur // 2, y), cv.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        distancePX = 0
        # Si on trouve deux points
        if (len(coordonneesCercle.keys()) == 2):
            # On dessine une ligne reliant les milieux des cercle étant eux mêmes au milieu des rectangles (besoin de les dessiner ??)
            cv.line(frame, coordonneesCercle[0], coordonneesCercle[1], (255, 0, 0), 2)
            # Calcul de la distance
            distancePX = np.sqrt((coordonneesCercle[0][0] - coordonneesCercle[1][0]) ** 2 + (
                        coordonneesCercle[0][1] - coordonneesCercle[1][1]) ** 2)
            # On écrit la distance sur la fenêtre
            # cv.putText(frame, str(distancePX), (640, 480), cv.FONT_HERSHEY_PLAIN, 1.0, (0, 255, 0), 2)

        # Si l'utilisateur a coché l'enregistrement de donnée et le test a été lancé
        if self.__indiceEnvoieDonnees and self.__indiceEnregistrementFrame:
            # On enregistre la frame
            self.enregristreFrame(frame)

        return (frame, masque), distancePX, dictPointTaille, coordonneesCercle

    def adapteZone(self, coordonneesCercle):
        """
        Méthode permettant d'adapter les dimensions de la fenêtre d'affichage en fonction de la position de la marque la plus haute

        Paramètre :
            - coordonneesCercle : Dictionnaire comportant les coordonnées des deux groupes de pixels représentant les marques.
        """
        # Si j'ai bien deux marques
        if len(coordonneesCercle) == 2:
            # Si la coordonnée en y d'une des marques est plus petite
            if coordonneesCercle[0][1] < coordonneesCercle[1][1]:   # Si c'est plus petit c'est plus haut sur l'image
                # Si la distance séparant le haut de l'image et le centre de la marque est inférieur à 100 pixels
                if coordonneesCercle[0][1] < 100:
                    # On ajoute 4 pixels
                    self.setROIDeformation(4)
                else:
                    # On retire 4 pixels
                    self.setROIDeformation(-4)
            else:   # Même principe avec l'autre marque si cette dernière est plus en haut
                if coordonneesCercle[1][1] < 100:
                    self.setROIDeformation(4)
                else:
                    self.setROIDeformation(-4)


    def enregristreFrame(self, frame):
        """
        Méthode permettant d'enregistrer la frame actuelle en ajoutant un label d'indication de temps en bas à gauche de l'image.

        Paramètre :
            - frame : Image actuelle du rendu vidéo.
        """
        # On ajoute l'indication de temps
        cv.putText(frame, str(self.__temps),
                   (10, frame.shape[0] - 10),
                   cv.FONT_HERSHEY_PLAIN,
                   1.0,
                   (0, 255, 255),
                   1)
        # On enregistre la frame dans le dossier dédié
        cv.imwrite(self.__dossierFrame + '/frame_' + str(self.__temps) + '.jpg', frame)


    def stop(self):
        self.isRunning = False
        self.quit()
        self.terminate()

    """ GETTER ET SETTER """

    def getNumCam(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant au numéro de la source vidéo.

        Return :
            - La valeur de l'attribut privé __numCam.
        """
        return self.__numCam
    def setNumCam(self, numCam):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant au numéro de la source vidéo.

        Paramètre :
            - numCam : Numéro de la source vidéo
        """
        self.__numCam = numCam

    def setDistance(self, distance):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à la valeur en pixels de la distance.

        Paramètre :
            - distance : Distance en pixels.
        """
        self.__distance = distance
    def getTailleContours(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant au dictionnaire avec la taille en pixels des contours des groupes de pixels.

        Return :
            - La valeur de l'attribut privé __tailleContours.
        """
        return self.__tailleContours
    def getMinimumTaille(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à la taille miniùame en pixels pour considérer un groupe de pixels.

        Return :
            - La valeur de l'attribut privé __minimumTaille.
        """
        return self.__minimumTaille
    def setMininumTaille(self, minimumTaille):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à la taille minimale en pixels pour considérer un groupe de pixels.

        Paramètre :
            - minimumTaille: Valeur de la taille minimale en pixels.
        """
        self.__minimumTaille = minimumTaille
    def getHue(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à la valeur de la hue.

        Return :
            - La valeur de l'attribut privé __hue.
        """
        return self.__hue
    def setHue(self, hue):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à la valeur de la hue.

        Paramètre :
            - hue: Nouvelle valeur de la hue.
        """
        self.__hue = hue
    def getSaturation(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à la valeur de la saturation.

        Return :
            - La valeur de l'attribut privé __saturation.
        """
        return self.__saturation
    def setSaturation(self, saturation):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à la valeur de la saturation.

        Paramètre :
            - saturation: Nouvelle valeur de la saturation.
        """
        self.__saturation = saturation
    def getValue(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à la valeur de la value.

        Return :
            - La valeur de l'attribut privé __value.
        """
        return self.__value
    def setValue(self, value):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à la valeur de la value.

        Paramètre :
            - value: La nouvelle valeur de la value.
        """
        self.__value = value
    def getIndiceCouleurMarques(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à l'indice de la couleur des marques à reconnaître.

        Return :
            - La valeur de l'attribut privé __indiceCouleurMarques.
        """
        return self.__indiceCouleurMarques
    def setIndiceCouleurMarques(self, indiceCouleurMarques):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à l'indice de la couleur des marques à reconnaître.

        Paramètre :
            - indiceCouleurMarques : Nouvelle valeur pour l'indice de couleur des marques. 0 --> Noir | 1 --> Blanc
        """
        self.__indiceCouleurMarques = indiceCouleurMarques

    def getIndiceEnregistrementFrames(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à l'indice permettant de savoir si le logiciel doit enregistrer les frames.

        Return :
            - La valeur de l'attribut privé __indiceEnregistrementFrame.
        """
        return self.__indiceEnregistrementFrame
    def setIndiceEnregistrementFrames(self, indiceEnregistrementFrames):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à l'indice permettande de savoir si le logiciel doit enregistrer les frames.

        Paramètre :
            - indiceEnregistrementFrames : Nouvelle valeur de l'indice permettant de savoir si le logiciel doit enregistrer les frames.
        """
        self.__indiceEnregistrementFrame = indiceEnregistrementFrames
    def getIndiceEnvoieDonnees(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à l'indice permettant de savoir si le logiciel doit commencer l'envoi des données.

        Return :
            - La valeur de l'attribut privé __indiceEnvoieDonnées.
        """
        return self.__indiceEnvoieDonnees
    def setIndiceEnvoieDonnees(self, indiceEnvoieDonnees):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à l'indice permettant de savoir si le logiciel doit commencer l'envoi des données.

        Paramètre :
            - indiceEnvoieDonnees : Nouvelle valeur de l'indice permettant de savoir si le logiciel doit commencer l'envoi des données.
        """
        self.__indiceEnvoieDonnees = indiceEnvoieDonnees
    def setTemps(self, temps):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à l'attribut du temps.

        Paramètre :
            - temps : Nouvelle valeur de l'attribut correspondant à l'attribut du temps.
        """
        self.__temps = temps
    def setReductionDeBruit(self, reductionDeBruit):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à l'indice permettant de savoir si le logiciel doit activer la réduction de bruit.

        Paramètre :
            - reductionDeBruit : Nouvelle valeur de l'indice permettant au logiciel de savoir s'il doit activer la réduction de bruit.
        """
        self.__indiceReductionDeBruit = reductionDeBruit

    def getFPS(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant au nombre de frames par secondes, pour l'instant inutile.

        Return :
            - La valeur de l'attribut privé __fps.
        """
        return self.__fps

    def getDistance(self):
        """
        Getter permettant d'accéder à la valeur stockée dans l'attribut correspondant à la distance en pixels mesurée.

        Return :
            - L'attribut __distance.
        """
        return self.__distance
    def setFPS(self, fps):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à la valeur du nombre de frames par secondes.

        Paramètre :
            - fps : Nouvelle valeur du nombre de frames par secondes.
        """
        self.__fps = fps

    def getIndiceSelectionManuelleFinie(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à l'indice permettant au logiciel de savoir si la sélection manuelle est finie.

        Return :
            - La valeur de l'attribut privé __indiceSelectionFinie.
        """
        return self.__indiceSelectionFinie

    def setIndiceSelectionManuelle(self, valeur):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant à l'indice permettant au logiciel de savoir si la sélection manuelle est finie.

        Paramètre :
            - valeur : Nouvelle valeur de l'indice permettant au logiciel de savoir si la sélection manuelle est finie.
        """
        self.__indiceSelectionAuto = valeur

    def setROIDeformation(self, deformation):
        """
        Méthode permettant de gérer la déformation de l'image rognée. En fonction de la position de la marque la plus haute,
        la méthode va agrandir ou rapetissir le rendu vidéo.

        Paramètre :
            - deformation : Indice de déformation. Correspond au nombre de pixels que l'utilisateur souhaite ajouter ou enlever au rendu vidéo.
        """
        # On cherche quelle marque est la plus en haut de l'image
        if dataRoi['position1'][1] < dataRoi['position2'][1]:
            verificationDepassementImage = dataRoi['position1'][1] - deformation
            # On regarde si on peut modifier l'image sans dépasser la dimension maximale du rendu vidéo
            if verificationDepassementImage > 0:
                nouvelY = dataRoi['position1'][1] - deformation
                x = dataRoi['position1'][0]
                # On change la valeur y du dictionnaire de dimension du nouveau rendu vidéo
                dataRoi['position1'] = (x, nouvelY)
        else:
            verificationDepassementImage = dataRoi['position2'][1] - deformation
            if verificationDepassementImage > 0:
                nouvelY = dataRoi['position2'][1] - deformation
                x = dataRoi['position2'][0]
                dataRoi['position2'] = (x, nouvelY)

    def setDossierFrame(self, chemin):
        """
        Setter permettant de mettre à jour la valeur de l'attribut correspondant au chemin vers le dossier stockant les frames.

        Paramètre :
            - chemin : Nouveau chemin vers le dossier stockant les frames.
        """
        self.__dossierFrame = chemin

    def getNoSignal(self):
        """
        Getter permettant d'avoir accès à la valeur stockée dans l'attribut correspondant à l'indicateur de la présence d'un signal vidéo ou non.

        Return :
            - La valeur de l'attribut privé __noSignal.
        """
        return self.__noSignal