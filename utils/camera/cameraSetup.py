from pygrabber.dshow_graph import FilterGraph

class ChoixCamera():
    """
    Classe permettant d'avoir accès aux périphériques vidéos sur l'ordinateur de l'utilisateur. Cela permettra au
    logiciel de donner le choix de la source vidéo à utiliser pour la pratique du programme.
    """
    def getVideoSources(self):
        """
        Méthode permettant de détecter les différentes sources vidéos sur l'ordinateur et de les lister. Le numéro
        associé à chaque source correspond à son numéro sur OpenCV.

        Return :
            - Dictionnaire regroupant les différentes sources vidéos et leur numéro associé.
        """
        # Dictionnaire des sources vidéos.     Clé : str: Nom de la source | Valeur : int: numéro de la source
        mapCam = {}
        # On parcourt toutes les sources vidéos détectées
        for i in range(len(FilterGraph().get_input_devices())):
            # On ajoute la source vidéo au dictionnaire
            mapCam[FilterGraph().get_input_devices()[i]] = i
        return mapCam