import nidaqmx
from nidaqmx.constants import TerminalConfiguration


class VoltManager:
    """
    Classe permettant de gérer la l'envoi de la tension à la carte analogique. La carte analogique utilisée pour le
    développement du logiciel est NI USB-6003, les différentes valeurs minimales et maximales correspondent donc aux
    valeurs de la carte et peuvent être changées.
    """
    __deformationMax = None
    __maxVal = None
    __valeurEnvoyee = 0
    def __init__(self, nomAppareil: str, min: float = 0, max: float = 10, deformationMax: float = None):
        """
        Paramètres :
            - nomAppareil : Nom de l'appareil lorsqu'il est reconnu par l'ordinateur (ex : Dev1, Dev2, ...).
            - min : Valeur minimale supportée par la carte (de base 0).
            - max : Valeur maximale supportée par la carte (de base 10).
            - deformationMax : Valeur de la déformation maximale par rapport au profil sur le fichier de configuration.
        """
        self.task = nidaqmx.Task()
        self.channel = nomAppareil
        self.__maxVal = max
        self.task.ao_channels.add_ao_voltage_chan(nomAppareil, min_val=min, max_val=max)
        self.taskRead = nidaqmx.Task()
        self.taskRead.ai_channels.add_ai_voltage_chan("Dev1/ai4", min_val=min, max_val=max)
        self.taskRead.ai_channels.add_ai_voltage_chan("Dev1/ai0", min_val=min, max_val=max,
                                                     terminal_config=TerminalConfiguration.DIFF)
        self.__deformationMax = deformationMax

    def setVoltage(self, deformation, typeDeformation):
        """
        Méthode permettant d'envoyée une valeur en tension en fonction de la déformation du matériau.

        Paramètres :
            - deformation : Valeur actuelle de la déformation mesurée par le programme.
        """
        # Calcul de la tension
        self.__valeurEnvoyee = (deformation / self.__deformationMax) * self.__maxVal
        if self.__valeurEnvoyee >= self.__maxVal:
            self.__valeurEnvoyee = self.__maxVal

        # Écriture de la tension
        self.task.write(self.__valeurEnvoyee)
        #results = self.taskRead.read()
        #print("Tension lue : ", results[1] - results[0])
        self.task.start()
        self.task.stop()

    def getValeurEnvoyee(self):
        """
        Getter permettant d'avoir accès à l'attribut correspondant à la valeur en tension envoyée à la carte analogique.

        Return :
            - L'attribut correspondant à la valeur en tension envoyée à la carte analogique.
        """
        return self.__valeurEnvoyee

    def close(self):
        """
        Méthode permettant de fermer la tâche associée à la carte analogique.
        """
        self.task.close()