class DataSortie:
    """
    Classe permettant de générer le fichier de sortie avec les données du test.
    """
    __chemin = 0
    def __init__(self, chemin, nomCamera, moyennage, distanceCameraEchantillon, distanceMarques):
        """
        Méthode de construction permettant de créer le fichier de sortie en fonction du chemin. Le chemin est
        le même pour chaque fichier, il se trouvera dans le dossier "output" regroupant tous les dossiers avec
        les fichiers de sortie. Le fichier de sortie se créera dans le dossier dédié aux tests lancés lors d'une
        même exécution du logiciel.

        Paramètres :
            - chemin: Chemin permettant de générer le fichier de sortie.
            - nomCamera: Nom de la caméra utilisée.
            - moyennage: Nombre de frames à moyenner.
            - distanceCameraEchantillon: Distance entre la caméra et l'échantillon.
            - distanceMarques: Distances entre les marques sur l'échantillon.
        """
        self.__chemin = chemin
        # Création du fichier de sortie
        _ = open(self.__chemin, "x")
        with open(chemin, "w") as fichier:
            # Écriture des informations supplémentaires dans l'entête du fichier
            fichier.write("****************\n")
            fichier.write("Nom camera : " + nomCamera + "\n")
            fichier.write("Moyennage : " + moyennage + "\n")
            fichier.write("Distance cam/ech : " + distanceCameraEchantillon + "\n")
            fichier.write("Distances marques : " + distanceMarques + "\n")
            fichier.write("****************\n")
            # Écriture des entêtes du fichier
            fichier.write("Temps, distance en centimetres, deformation, tension\n")

    def ecritureDonnees(self, temps, distanceCentimetres, deformation, tension):
        """
        Méthode permettant d'écrire les données sur le fichier de sortie dédié au test en cours.

        Paramètres :
            - temps : Indice en seconde du temps courant du test.
            - distanceCentimetres : Distance en centimètres entre les deux marques détectées.
            - deformation : Déformation enregistrée en fonction du L0 renseigné par l'utilisateur.
            - tension : Tension envoyée à la carte analogique.
        """
        with open(self.__chemin, "a") as fichier:
            # Écriture des données
            fichier.write(
                str(temps) + ', ' +
                str(distanceCentimetres) + ', ' +
                str(deformation) + ', ' +
                str(tension) + '\n'
            )
            fichier.close()