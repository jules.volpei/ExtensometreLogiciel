import yaml

class YamlLoader():
    """
    Classe permettant d'avoir accès aux méthodes pour écrire et lire le fichier de configuration YAML.
    """
    fichier = 'utils/data/config.yaml'
    cles = None
    def getYaml(self):
        """
        Retourne le contenu du fichier de configuration Yaml.

        Return :
            - Contenu du fichier de configuration Yaml.
        """
        with open(self.fichier, 'r') as file:
            return yaml.safe_load(file)

    def ecritDoc(self, data):
        """
        Écrit dans le fichier de configuration Yaml un nouveau dictionnaire de configuration pour remplacer l'ancien.

        Paramètre :
            - data : Nouveau fichier de configuration.
        """
        with open(self.fichier, 'w') as file:
            file.write(yaml.dump(data, default_flow_style=False))

    def modifieValeurPixel(self, distancePX, cleProfil):
        """
        Méthode permettant de modifier la valeur de la distance en pixels dans le fichier de configuration Yaml.

        Paramètres :
            - distancePX : Nouvelle distance en pixels.
            - cleProfil : Clé permettant d'accéder au profil de configuration.
        """
        print(distancePX)
        file = self.getYaml()
        file[cleProfil]['pixel'] = float(distancePX)
        self.ecritDoc(file)

    def getKeys(self):
        with open(self.fichier, 'r') as file:
            data = yaml.safe_load(file)
        self.cles = list(data.keys())

    def modifieProfil(self, cleProfil, distanceMarques, distanceCamera, distancePX, h, s, v, moyennage, fps, couleurMarques, y1, y2, deformationMax, typeDeformation):
        fichier = self.getYaml()
        fichier[cleProfil].update({
                        'distanceMarques': float(distanceMarques),
                        'distanceEnchantillonCamera': int(distanceCamera),
                        'distancePixels': float(distancePX),
                        'hsv': {
                            'Hue': int(h),
                            'Saturation': int(s),
                            'Value': int(v)
                        },
                        'moyennage': moyennage,
                        'fps': int(fps),
                        'couleurMarques': int(couleurMarques),
                        'points': {
                            'y1': float(y1),
                            'y2': float(y2)
                        },
                        'deformation': {
                            'max': deformationMax,
                            'type': typeDeformation
                        }
                    })
        self.ecritDoc(fichier)

    def creeNouveauProfil(self, cleProfil, distanceMarques, distanceCamera, distancePX, h, s, v, moyennage, fps, couleurMarques, y1, y2, deformationMax, typeDeformation):
        """
        Méthode permettant la création d'un nouveau profil par l'utilisateur.

        Paramètres :
            - cleProfil : Nom du profil crée par l'utilisateur. Cette valeur va également servir de clé pour le fichier de configuration.
            - distanceMarques : Distance entre les deux marques dessinées sur l'échantillon de test. (Distance en centimètres).
            - distanceCamera : Distance entre la caméra et la machine de traction. (Distance en centimètres).
            - distancePX : Distance en pixels calculée par le logiciel.
            - h : Valeur de la hue.
            - s : Valeur de la saturation.
            - v : Valeur de la valeur.
            - moyennage : Présence de moyennage ou non pour le test.
            - fps : Nombre de frames par seconde (pas encore implémentée, 20 par défaut).
            - couleurMarques : Couleur des marques (0 --> marques noires | 1 --> marques blanches).
            - y1 : Taille actuelle du groupe de pixels Y1 mesurée sur le logiciel.
            - y2 : Taille actuelle du groupe de pixels Y2 mesurée sur le logiciel.
            - deformationMax : Déformation maximale du matériau.
            - typeDeformation : Type de déformation voulue par l'utilisateur.
        """
        test = {cleProfil:
                    {
                        'distanceMarques': float(distanceMarques),
                        'distanceEnchantillonCamera': int(distanceCamera),
                        'distancePixels': float(distancePX),
                        'hsv': {
                            'Hue': int(h),
                            'Saturation': int(s),
                            'Value': int(v)
                        },
                        'moyennage': moyennage,
                        'fps': int(fps),
                        'couleurMarques': int(couleurMarques),
                        'points': {
                            'y1': float(y1),
                            'y2': float(y2)
                        },
                        'deformation': {
                            'max': deformationMax,
                            'type': typeDeformation
                        }
                    }}
        # Ajout du profil dans le fichier Yaml
        fichier = self.getYaml()
        fichier.update(test)
        self.ecritDoc(fichier)