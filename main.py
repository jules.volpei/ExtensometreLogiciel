from PyQt5 import QtWidgets
import sys

import utils.data.yamlParser as yamlParser
import utils.videothread.videothread as videothread
import utils.gui.maquetteV3 as gui

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = gui.MainWindow()
    ui.setupUi(Dialog, videothread.VideoThread(fps=20), yamlParser.YamlLoader())
    Dialog.show()
    sys.exit(app.exec_())  # Ça ça bousille le programme